import requests
import urllib.request
import json

# -------------------------
FILE_PATH = "images/"
INSTAGRAM_URL = "https://www.instagram.com/p"
DOWNLOAD_URL = "media/?size=l"
JSON_FILE_NAME = "image_labels_data_sample.json"
# -------------------------

# -------------------------
'''
Function for download image from Internet with URL 
'''
def image_loader(post_url, filename):

    picture_url = '{}/{}/{}'.format(INSTAGRAM_URL, post_url, DOWNLOAD_URL)

    file_name = 'image-{}.jpg'.format(filename)
    full_path = '{}{}'.format(FILE_PATH, file_name)

    try:
        urllib.request.urlretrieve(picture_url, full_path)
    except Exception as ex:
        print(ex)
        pass

    print('{} saved.'.format(file_name))

'''
Function for split json file and use imageloader 
'''
def load_images_from_JSON(filename):

    with open(filename, 'r') as f:
        json_data = json.load(f)

    post_url = [] # contain post all posts URL
    post_labels = []

    i = 0 # image name

    for i, sample in enumerate(json_data):

        post_url.append(sample['postid'])

        post_labels.append(sample['prediction'])

        image_loader(sample['postid'], i) # load image from post, i - file_name

def main():
    load_images_from_JSON(JSON_FILE_NAME)

if __name__ == "__main__":
    main()
